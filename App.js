import React, { Component } from "react";
import { Button, StyleSheet, View, Text, TextInput } from "react-native";

export default class App extends Component {
  state = { counter: 0,text:'' };
  render() {
    const { counter } = this.state;
    return (
      <View
        style={LOCAL_STYLES.wrapper}
        testID="app-root"
        accessibilityLabel="app-root">
        <TextInput testID="test-input" accessibilityLabel="test-input" style={{ height: 40, borderColor: 'gray', borderWidth: 1 ,width:200}}
        onChangeText={text => this.setState({text:text})}
        value='' />
        <Text testID="test-label" accessibilityLabel="test-label">Test Label</Text>
        <Button
          title={"Clicked " + counter + " times"}
          testID="increase-count"
          accessibilityLabel="increase-count"
          onPress={() => {
            this.setState({ counter: counter + 1 });
          }}
        />
      </View>
    );
  }
}

const LOCAL_STYLES = StyleSheet.create({
  wrapper: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center"
  }
});