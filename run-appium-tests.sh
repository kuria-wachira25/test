#!/bin/bash
react-native bundle --dev false --platform android --entry-file App.js --bundle-output ./android/app/src/main/assets/index.android.bundle --assets-dest ./android/app/src/main/res
cd android && ./gradlew assembleRelease
cd ..
wdio wdio.conf.js
